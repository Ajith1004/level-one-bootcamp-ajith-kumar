//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>
struct point
{
    float x;
    float y;
};
typedef struct point Points;

Points input()
{
    Points p;
    printf("enter x-co-ordinate:");
    scanf("%f",&p.x);
    printf("enter y-co-ordinate:");
    scanf("%f",&p.y);
    return p;
}
float distance(Points p1,Points p2)
{
    float x,y,dist;
    x=(p2.x-p1.x);
    y=(p2.y-p1.y);
    dist= sqrt ( ( x * x ) + ( y * y ) );
    return dist;
}
float output(Points p1,Points p2,float dist)
{
printf("the distance between (%f,%f) and (%f,%f) is : %f"  ,p1.x,p1.y,p2.x,p2.y,dist);
}

float main()
{

float dist ;
Points p1,p2;
p1=input();
p2=input();
dist=distance(p1,p2);
output(p1,p2,dist);
return 0;
}
