//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>
float input()
{
float a;
scanf("%f" ,&a);
return a;
}
float distance(float x1,float y1,float x2,float y2)
{
float x,y,dist;
x=(x2-x1);
y=(y2-y1);
dist= sqrt ( ( x * x ) + ( y * y ) );
return dist;
}
float output(float x1 ,float y1, float x2,float y2,float dist)
{
printf("the distance between (%f,%f) and (%f,%f) is : %f"  ,x1,y1,x2,y2,dist);
}
float main()
{

float x1,y1, x2,y2 ,dist ;
printf("enter the x1 coordinates:\n");
x1=input();
printf("enter the y1 coordinates:\n");
y1=input();
printf("enter the x2 coordinates:\n");
x2=input();
printf("enter the y2 coordinates:\n");
y2=input();
dist= distance(x1,y1,x2,y2);
output(x1,y1,x2,y2,dist);
return 0;
}
